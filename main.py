import os, pickle, sys, textwrap
from time import sleep

tcol = os.get_terminal_size()
screenWidth = tcol[0]
normal = 0.05
arrow = " 🠶 " # Some good exmaples I found: > » 🠶 $

def write(text, time=normal):
    texxt = textwrap.fill(text, screenWidth)
    for character in texxt:
        sys.stdout.write(character)
        sys.stdout.flush()
        sleep(time)
    print("")

def titleScreen():
    os.system('clear')
    print("  ╔═════════════╗")
    print("  |  adv≡nture  |")
    print("  ╚═════════════╝")
    write("   ~Bottom text~ \n")
    print("")
    sleep(0.08)
    print(" [start|help|quit]")
    write(" By Waces-----2018")

def mainLoop():
    while 1==1:
        print("")
        uip = input(arrow)
        if uip == "start":
            import resources
        if uip == "quit":
            os.system('clear')
            break

if __name__ == '__main__':
    titleScreen()
    mainLoop()
