# Text Adventure game
# CopyLEFT 2018 - Waces F.
#---Imports---
import cmd
import textwrap
import sys
# import pickle
import os
import random
from time import sleep

#---Some vars---
## Teminal size:
tcol = os.get_terminal_size()
screenWidth = tcol[0]
## write speeds
instant = 0.0001
faster = 0.009
fast = 0.02
normal = 0.05
slow = 0.07
slower = 0.2
slooow = 1

arrow = "»" # Some good exmaples I found: > » 🠶 $

jftsod = "Just for the sake of debugging."

#---Syntax list--- with models that are a list of words that can mean one thing
yesModel = ('yes','y','ye','yep','yup','yus','yis','s','sim','yee','yy')
#(N 0, S 1, E 2, W 3, NE 4,SE 5,SW 6,NW 7, in 8, out 9, up 10, down 11
#(North, South, East, West,NE,SE,SW,NW,    in,  out,    up,    down
nothModel = ["north",'n']
southModel = ['south','s']
eastModel = ['east','e']
westModel = ['west','w']
nestModel = ['northeast','north east','ne']
sestModel = ["southeast","south east","se"]
swstModel = ["southwest","south west","sw"]
nwstModel = ["northwest","north west","nw"]
inModel = ["inside","in",'i']
outModel = ["outside", "out",'o']
upModel = ["up",'u']
downModel = ["down",'dw','d']

def repEl(li, ch, num, repl):
    last_found = 0
    for _ in range(num):
        last_found = li.index(ch, last+found+1)
    li[li.index(ch, last_found)] = repl

#Directions in the list, yes I could have just used tuples, but whatever
dirN,dirS,dirE,dirW,dirNE,dirSE,dirSW,dirNW,dirIn,dirOut,dirUp,dirDown = 0,1,2,3,4,5,6,7,8,9,10,11

#Color class thing---
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

def bold(wrd):
    bldw = (color.BOLD,wrd,color.END)
    return "".join(bldw)

# ---Player class---
class Player:
    def __init__(self, name, startPos):
        self.name = name
        self.maxLife = 10
        self.life = self.maxLife
        self.attacks = []
        self.position = startPos
        self.inventory = []
        self.carryWeight = 10
        #Add self.atributeName = value(s) to add more values to player
    def getCarry(self):
        iw = 0
        for i in self.inventory:
            iw += i.weight
        return iw

    def sayHt(self):
        if self.life > 0:
            return self.life
        if self.life < 1:
            return 0

# Placeholder player:
# player = Player('Player', darkRoom0)
# player.inventory = ['readMe.md'] #Placeholder item

#---Dialogue tree/Dialogue nodes classes---
class DTree():
    def __init__(self):
        self.nodes = []
        self.options = []
        # Keep these like this and add other stuff bellow
        self.endCon = ("Goodbye","Farewell","It was a pleasure talking to you")
        self.sorry = ("Sorry?","Excuse me?","Sorry but I didn't understand you")

    def exNd(self,node,spd=normal): #Execute node
        if node in self.nodes:
            write(node.text,spd)
            for i in node.addO:
                if i not in self.options:
                    self.options.append(i)
    
    def pO(self):
        sae = ", ".join(self.options)
        sael = []
        sael.append("You can say: ")
        sael.append(sae)
        sael.append(f" or {bold('goodbye')}/{bold('farewell')}")
        eea = "".join(sael)
        write(eea)

    def conv(self, ttN):
        write("You are talking to %s"%(ttN),faster)
        print("")
        self.exNd(iniN)
        while True:
            print("")
            uip = input(arrow)
            if uip in self.options:
                for i in self.nodes:
                    if uip in i.call:
                        self.exNd(i)
            elif uip == 'what can I say' or uip == 'what can I say?':
                self.pO()
            elif uip == 'goodbye' or uip == 'farewell':
                write(random.choice(self.endCon),fast)
                break
            else:
                write(random.choice(self.sorry),fast)

class DNodes():
    def __init__(self,txt='none', cll='void/null'):
        self.text = txt
        self.addO = ()
        self.rmO = ()
        self.call = cll

# <dial1>
dial1 = DTree()

#iniN is the first dialogue in a converstion
iniN = DNodes(f"Hello there, ????, how can I {bold('help')} you today?")
iniN.addO = ["help"]

theH = DNodes(f"So, you are saying some {bold('strange things')} have been happening to you recently? Like {bold('what')}?", 'help')
theH.addO = ("strange things","what")

theST = DNodes(f"I mean, it's for things like these that you are under {bold('treatment')}, ???",'strange things')
theST.addO = ["treatment"]

theW = DNodes(f"Well, windows seemingly 'flickering' really sounds weird, but don't worry, you can share anything with me, it's just part of your {bold('treatment')}",'what')
theW.addO = ["treatment"]

theT = DNodes(f"Yes, Mr. ????, you've been under some treatment after the {bold('incident')}, not like you didn't know that",'treatment')
theT.addO = ['incident']

theI = DNodes(f"Yes, yes, the 'incident', we could discuss it, but it always seems you start to get a bit unconfortable when sharing about it\nSeems like you may need some more resting for today, just go home and try relaxing, {bold('farewell')}.",'incident')

dial1.nodes = (iniN,theH,theST,theW,theT,theI)
# </dial1>

#---NPC class---
class NPC():
    def __init__(self,unknownName, knownName,dialogueTree,uws):
        self.uName = unknownName #The name that will show when you haven't talked to them
        self.kName = knownName #Their name after you talked to them
        self.name = self.uName
        self.uWords = uws #What you will say to refer to them when you don't know
        self.kWords = () #And when you know
        self.words = self.uWords
        self.dialogue = dialogueTree
    def talkTo(self):
        self.dialogue.conv(self.name)
        self.name = self.kName
        self.words = self.kWords

drUn = NPC("Doctor ???","Dr. Vargs",dial1,("doctor","dr","???","dr_???")) 
drUn.kWords = ("doctor","dr","dr_vargs","dr._vargs","vargs")

#---Attcks class---Ehhh
class Attk:
    def __init__(self, name, daMin, daMax):
        self.name = name
        self.text = " "
        self.damIn = daMin
        self.damAx = daMax
#---Atks-
punch0 = Attk("punch", 1, 4)

#---Enemy Class--- W.I.P
class Enemy:
    def __init__(self, name, sname, life, res):
        self.name = name #Name of it in the room
        self.selfName = sname #The other name, idk
        self.desc = " " # A description of the enemy
        self.grit = " " #Some line by the enemy
        self.life = life
        self.drop = ()
        self.respawn = res #True or False
        self.dead = False #If this enemy has EVER been killed
        self.spawned = False #If this enemy has spawned
        self.attacks = ()
        self.words = ()
    def attac(self):
        at = random.choice(self.attacks)
        dam = random.randrange(at.damIn, at.damAx)
        player.life -= dam
        liner = (self.selfName, "attacked you with", at.name, "and gave", str(dam), "damage.")
        linToS = " ".join(liner)
        print("")
        write(linToS ,fast)
        write("You have %i/%i of health now." %(player.sayHt(),player.maxLife), fast)

#---Enemies-
mon0 = Enemy('a monster', "monster", 5, True)
mon0.grit = "WRAAAHHH!"
mon0.desc = "A generic monster for testing purposes."
mon0.attacks = [punch0]
mon0.words = ['monster']

#---Objects class---
class Object:
    def __init__(self, name, grounDescription, shortDesc, takeBool):
        self.name = name
        self.words = () # Other words that mean it
        self.groundDesc = grounDescription # What shows when you enter room/look around if it still on the floor
        self.longDesc = ""
        self.shortDesc = shortDesc
        self.usable = False # If this object can be used
        self.useResult = ()
        self.readable = False
        self.texto = ""
        self.pickable = takeBool # If it can be picked by the player
        self.weight = 1
        self.size = 'medium' #Can be small medium large
        self.tipo = 'object'
        self.otus = () #Object(s) to use with
    def useI(self, inp):
        if inp == self.otus:
            self.useResult()
        else:
            write("Seem like nothing happened.")

#---Objects---
testObj = Object("testObj", "There is an undefined test object.", "A test object",True)
testObj.longDesc = "It is an object made to test the"
testObj.words = ['test object','testobj']

readMe = Object("readMe.md",
                "in this area, lays a strange paper with something written",
                "A paper note with square cuts at the bottom",
                True)
readMe.words = ['readme','note','readme.md']
readMe.readable = True
readMe.longDesc = "It is a note with some square cuts at the bottom."
readMe.texto = "It reads 'Hello World'."
readMe.weight = 1

woodTable = Object("Wooden table",
                   "I see a short wooden table by the east wall",
                   "A wooden table",
                   False)
woodTable.longDesc = woodTable.shortDesc
woodTable.words = ['table','wooden table']

key0 = Object("Rusty key",
              "looking at the floor I see a rusty key laying here",
              "It's a rusty key",
              True)
key0.longDesc = "It is a, old rusty key, it may serve to open a door"
key0.words = ['key','rusty key']
key0.weight = 2
key0.otus = readMe

lapTop = Object("Laptop",
                "I see a laptop laying on the floor",
                "A laptop laying on the floor that seems to be On",
                False)
lapTop.longDesc = "It's a laptop laying on the ground, and seems to be On. There's something displayed on its screen, two terminals, one running some text editor and the other running an unknown program."
lapTop.texto = "The time and date shown are 18:30-2-Feb-2000, on the first panel, it reads 'This is the end of the TEST.', on the other terminal panel, it reads\n'West of house\nThis is an open field west of a white house, with a boarded front door.\nThere is a small mailbox here', the bellow thext is obscured."
lapTop.readable = True
lapTop.words = ['laptop', 'computer','notebook','machine','dell inspiron 14r']

pic0 = Object('Old photo', 'I can see a small paper photograph laying on something, maybe the floor', 'It is an old paper photo', True)
pic0.words = ['photograph', 'photo','picture']
#--- Class Storage
class Storage(Object):
    def __init__(self, name, grounDescription, shortDesc, takeBool):
        self.name = name
        self.words = () # Other words that mean it
        self.longDesc = ""
        self.shortDesc = shortDesc
        self.usable = False # If this object can be used
        self.useResult = ()
        self.readable = False
        self.texto = ""
        self.pickable = takeBool # If it can be picked by the player
        self.weight = 1
        self.groundDesc = grounDescription
        self.size = 'medium'

        self.contents = [] #Ohter items that it contains
        self.sizes = ('small','medium','large')

        self.tipo = 'Container'

    def openIt(self):
        write('You open %s, and see'%self.name)
        if len(self.contents) > 0:
            for item in self.contents:
                print('[%s Kg] %s'%(item.weight ,item.name))
                sleep(fast)
        else:
                write("That it is empty.",fast)
        print("")
        print("take | put | look | close")
        while 1 == 1:
            print('')
            oIp = input("%s%s"%(self.name,arrow))
            print('')
            oisPlit = oIp.split()
            if oisPlit[0].lower() == 'take' and len(oisPlit) > 1:
                tto = "e"
                for i in self.contents:
                    if oisPlit[1].lower() in i.words:
                        tto = i
                if tto != "e":
                    if tto.weight <= (player.getCarry() + tto.weight):
                        player.inventory.append(tto)
                        self.contents.pop(self.contents.index(tto))
                        write("You took %s" %(tto.name))
                    else:
                        write('This is too heavy for you to get')
                elif tto == "e":
                    write("I didn't find this item here.")
            elif oisPlit[0].lower() == 'put' and len(oisPlit) > 1:
                tto = "e"
                for i in player.inventory:
                    if oisPlit[1].lower() in i.words:
                        tto = i
                if tto != "e":
                    if tto.size in self.sizes and tto != self:
                        self.contents.append(tto)
                        player.inventory.pop(player.inventory.index(tto))
                        write("You put %s here." %(tto.name))
                    else:
                        write("You can't put this here.")
                elif tto == "e":
                    write("I didn't find this item here.")
            elif oisPlit[0].lower() == "close":
                break
            elif oisPlit[0].lower() == "look":
                write('You look in %s again, and see'%self.name)
                if len(self.contents) > 0:
                    for item in self.contents:
                        print('[%s Kg] %s'%(item.weight ,item.name))
                        sleep(fast)
                else:
                        write("That it is empty.",fast)
        write("You close %s" %self.name)

#Storages
wallet0 = Storage('Leather wallet', "A small leather wallet lays on the floor", "A small leather wallet", True)
wallet0.contents = [pic0]
wallet0.words = ['wallet']

# Directions (North, South, East, West, NE,ES,SW,WN, in, out, up, down)

# Directions to 'not reachable' areas [wip to go with the 'go' commands]
class Blockage:
    blkgBool = True # Why does this even exists?
    def __init__(self, text):
        self.text = text #The text to say when you encounter it

wall = Blockage("There is a wall there.")
empty = Blockage("There is nothing there.")
ceiling = Blockage("There is the ceiling there.")
floor = Blockage("It is a solid floor.")
unknown = Blockage("I don't think it is possible.")
already = Blockage("I'm already here.")
blockageList = [wall,empty,ceiling,floor,unknown,already]
#---Door--- That only are used when there is a door to unlock
class Door:
    lockedText = "It seems to be locked"
    def __init__(self, lockedBool, r1, r2):
        self.name = "" #Name not needed
        self.locked = lockedBool

        self.lockedText = "There is a locked door here."
        self.connectedRooms = (r1,r2) #The 2 rooms that will be connected by this door
        self.keyItem = ()

#---Class Room---
class Room:
    def __init__(self, name, longDesc, shortDesc):
        self.name = name
        self.longDescription = longDesc
        self.dirs = ""
        self.shortDescription = shortDesc
        #                  The rooms/blockages in each direction
        #                   N,   S,   E,    W,  NE,  ES,  SW,  WN,  in,   out,  up,     down - 11
        self.connections = (wall,wall,wall,wall,wall,wall,wall,wall,empty,empty,ceiling,floor)
        self.items = [] # Make it like the inventory(?)
        self.beento = False
        self.enes = [] #The enemies rn
        self.enToSp = () #The enemies that can spawn
        self.spChance = 50 #Chance of spawning enemies in a room (min. 3)
        self.npcs = []

    def descItems(self):
        if len(self.items) > 0:
            for item in self.items:
                print("")
                print(item.grioundDesc)

    def itemsInDesc(self):
        id = []
        for i in self.items:
            id.append(i.groundDesc)
        if len(id) > 0:
            tS = ", ".join(id)
            ttS = [self.longDescription, tS, self.dirs]
            #print(ttS[0],ttS[1],ttS[2])
            tSr = ", ".join(ttS)
            #print(tSr)
            write(tSr ,fast)
        else:
            ttS = [self.longDescription, self.dirs]
            tSr = ", ".join(ttS)
            write(tSr, fast)

    def eneSpawn(self):
        sC = random.randrange(0, self.spChance) #The chance of something spawning, make a bigger gap for fewer chances
        if sC == 1:
            if len(self.enToSp) > 0:
                en = random.choice(self.enToSp)
                if en.dead == False and en.spawned == False:
                    self.enes.append(en)
                    en.spawned = True
                elif en.dead == True and en.respawn == True and en.spawned == False:
                    self.enes.append(en)
                    en.dead = False
                    en.spawned = True

    def eneKill(self, en):
        self.enes.pop(self.enes.index(en))
        en.spawned = False
        en.dead = True

    def eneDespall(self):
        for i in self.enes:
            self.enes.pop(self.enes.index(i))
            i.spawned = False

    def descen(self):
        if len(self.enes) > 0:
            for i in self.enes:
                nm = i.name
                tx = "!"
                tuo = (nm, tx)
                texy = "".join(tuo)
                write(texy, fast)

    def enAts(self):
        for en in self.enes:
            en.attac()

    def sayNpc(self):
        ns = []
        for i in self.npcs:
            ns.append(i.name)
        ens = ", ".join(ns)
        toss = []
        toss.append("I can see")
        toss.append(ens)
        toss.append("here.")
        ttoss = " ".join(toss)
        if len(self.npcs) > 0:
            write(ttoss)

#---Rooms/Doors--- # Doors are placed after the two rooms you want to connect
darkRoom0 = Room("Dark room",
                "I'm in a room with a very dim light source that comes from above",
                "It is a room with dim light.")
darkRoom0.dirs = "there is a door to somewhere to the North."

dripRoom1 = Room("Puddle room",
                "I'm in a room with faded light and a puddle formed with water dripping from the ceiling",
                "It's a room with water dripping from the ceiling.")
dripRoom1.dirs = "to the South there is a door to a dark room, to the West there is a door to somewhere and another door to the North."

key_Room2 = Room("Strange room",
                "I'm in an oddly feeling room",
                "It's an odd room, for some reason.")
key_Room2.dirs = "there is a door to another room to the East"

finlRoom3 = Room("Marble room",
                "I'm in a room with marble beige walls",
                "It is a room with a laptop on the floor.")
finlRoom3.dirs = "there is a door leading to another room to the South."
finlRoom3.enToSp = [mon0]
finlRoom3.npcs = [drUn]

lckdDoor0 = Door(True, finlRoom3, dripRoom1)
lckdDoor0.keyItem = key0

#                       (North0, South1, East2, West3, NE,ES,SW,WN, in, out, up, down)
darkRoom0.connections = (dripRoom1,wall,wall,wall,wall,wall,wall,wall,empty,unknown,ceiling,floor)
dripRoom1.connections = [lckdDoor0, darkRoom0, wall, key_Room2,wall,wall,wall,wall,already,unknown,ceiling,floor]
dripRoom1.items = [woodTable]

key_Room2.connections = (wall,wall,dripRoom1,wall,wall,wall,wall,wall,already,unknown,ceiling,floor)
key_Room2.items = [wallet0]

finlRoom3.connections = (wall,dripRoom1,wall,wall,wall,wall,wall,wall,empty,unknown,ceiling,floor)
finlRoom3.items = [lapTop]
# locked door list
lockedList = [lckdDoor0]

#--- Special uses for items

# Key
key0.usable = True
def k0r():
    if player.position == dripRoom1:
        #repEl(dripRoom1.connections, lckdDoor0, 0, finlRoom3)
        print("Please don't.")
    else:
        print("I can't use this here.")

key0.useResult = k0r
# Laptop
lapTop.usable = True
def l0t():
    write("On the strange program terminal, you write 'quit' and press enter.",0.05)
    print("")
    write("the program returns 'Are you sure you want to quit?', you write 'y' and press enter.", 0.05)
    print("")
    write("The program suddently exits-",0.04)
    sleep(1)
    print("")
    os.system("clear")
    exit()
lapTop.useResult = l0t
#---Map--- Model of game map
#  W← N↑ S↓ E→
#
#          [4]
#           ↑  Locked door (key on [3] )
#       [3][2]
#          [1] Start room
#
#---Turn Update---
def turnUpdate():
    #At every turn...
    player.position.enAts()
    #> Check if dead:
    if player.life < 1:
        sleep(0.7)
        print("")
        write("You died.", slower)
        print()
        sleep(2)
        exit() #Off jftsod

#---Call player---
player = Player('Player', dripRoom1) #Placeholder
player.inventory = [readMe, key0]
player.position.beento = True
player.attacks = [punch0]

#---Cmd class--- # Don't even ask me how this works
class userInput(cmd.Cmd):
    prompt = ("\n%s"%arrow) # Some good exmaples I found: > » 🠶 $

    def default(self, arg): #What to say when there is no known command
        print("Sorry, I can't understand that | Type 'help' for command list")

    def do_quit(self, arg):
        """Quit the game."""
        write('\nAre you sure? All unsaved progress will be lost.',faster)
        quiOpt = input('[yes/no]»')
        if quiOpt.lower() in yesModel:
            return True


    def help_save(self):
        print("Save system is [WIP].")

    def help_conversation(self):
        write(f"While talking to someone, some of their words will be {bold('bold')}, meaning that they are keywords, and can be used to navigate the dialigue, if you ever don't know what to say, just type 'what can I say' to have a list of all keywords.",instant)
   

#WIP HELP THAT USES WRITE()
#    def do_help(self, arg):
#        if arg.lower() == 'conversation':
#            self.help_conversation()
#        else:
#            write("To find help about some topic, use 'help <topic>'.",instant)
#            print("")
#            write("Commands:",instant)
#            write("look, go, take/get, talk, open, read, use, attack, drop",instant)
#            print("")
#            write("Other topics:",instant)
#            write("conversation, save",instant)


    def do_look(self, arg):
        """Use 'look around' to show what's around you,
        use 'look inventory' to show your items and
        use 'look at <obj>' to look at an specific object."""
        lookAt = arg.split()

        if arg.lower() == 'around':
            player.position.itemsInDesc()
            player.position.sayNpc()
            #print(" ")
            player.position.descen()
            #print(player.position.longDescription)
            #pr = player.position
            #pr.descItems()
        elif arg.lower() == 'inventory':
            if len(player.inventory) > 0:
                write("In your inventory you have:", fast)
                for item in player.inventory:
                    print('[%s Kg]'%item.weight ,item.name)
                    sleep(fast)
            else:
                print("Inventory is empty.")
        elif len(lookAt) > 0 and lookAt[0] == 'at':
            if len(lookAt) > 1:
                pr = player.position
                lookAt.pop(0)
                lit = ' '.join(lookAt)
                inRes = 7
                gdRes = 7
                #print(player.inventory)
                #player.lookObIn(lit)
                #pr.lookObGd(lit)
                for i in player.inventory:
                    #print(i)
                    #print(i.name)
                    ene = {'ld':i.longDesc,'wd':i.words}
                    #print(ene)
                    if lit in ene['wd']:
                        print(ene['ld'])
                        inRes = 10
                for i in pr.items:
                    #print(i)
                    #print(i.name)
                    ene = {'ld':i.longDesc,'wd':i.words}
                    #print(ene)
                    if lit in ene['wd']:
                        print(ene['ld'])
                        gdRes = 20
                if inRes + gdRes == 14:
                    print("I can't seem to find this item.")

            else:
                write("Look at what?")
        elif arg == "":
            player.position.itemsInDesc()
            player.position.sayNpc()
            #print(" ")
            player.position.descen()
        else:
            write("Sorry, but I could not understand this, maybe try using 'help look'.", fast)
        turnUpdate()

    def do_go(self, arg): # WIP
        """This command moves the player to a certain direction.
The directions are North, South, East, West, Notheast,
Southeast, Southwest, Northwest, In, Out, Up and Down. Use 'go <dir>'"""
#(N 0, S 1, E 2, W 3, NE 4,SE 5,SW 6,NW 7, in 8, out 9, up 10, down 11
        goDir = arg.lower() #lower() to make everything lowercase
        doGoDir = 100 # failsafe, kinda

        if goDir in nothModel: # Directions:
            doGoDir = dirN
            goTex = "north"
        if goDir in southModel:
            doGoDir = dirS
            goTex = "south"
        if goDir in eastModel:
            doGoDir = dirE
            goTex = "east"
        if goDir in westModel:
            doGoDir = dirW
            goTex = "west"
        if goDir in nestModel:
            doGoDir = dirNE
            goTex = "north east"
        if goDir in sestModel:
            doGoDir = dirSE
            goTex = "south east"
        if goDir in swstModel:
            doGoDir = dirSW
            goTex = "south west"
        if goDir in nwstModel:
            doGoDir = dirNW
            goTex = "north west"
        if goDir in inModel:
            doGoDir = dirIn
            goTex = "inside"
        if goDir in outModel:
            doGoDir = dirOut
            goTex = "out"
        if goDir in upModel:
            doGoDir = dirUp
            goTex = "up"
        if goDir in downModel:
            doGoDir = dirDown
            goTex = "down"

        if doGoDir < 100: #the, kinda, ailsafe
            if player.position.connections[doGoDir] not in blockageList:
                if player.position.connections[doGoDir] in lockedList: #if it's a locked door
                    print(player.position.connections[doGoDir].lockedText)
                else: #If it's a valid room
                    #player.position.eneDespall()
                    player.position = player.position.connections[doGoDir]
                    write("You go %s and get to %s." %(goTex,player.position.name), faster)
                    #print("")
                    if player.position.beento == False:
                        player.position.itemsInDesc()
                        player.position.beento = True
                    player.position.eneSpawn()
                    #print(" ")
                    player.position.sayNpc()
                    player.position.descen()
                    #print(player.position.longDescription)
                    #pr = player.position
                    #pr.descItems()
            else: #Print the blockage text
                print(player.position.connections[doGoDir].text)
        else:
            print("Invalid direction, please use 'help go' to see list of directions.")
        turnUpdate()

    def do_take(self, arg):
        """This takes an item from the room you are, if it is able to be taken. Use 'take <obj>'."""
        pr = player.position
        #print(len(pr.items))
        if len(pr.items) > 0:
           for i in pr.items:
               #print(i.words)
               if arg in i.words:
                   if i.pickable == True:
                       if key0.weight + player.getCarry() < player.carryWeight:
                            player.inventory.append(i)
                            write("You got %s" %i.name, faster)
                            pr.items.pop(pr.items.index(i))
                       else:
                            write("I'm too full to carry this.",0.05)
                   else:
                       print("It's better to keep this where it is.")
               else:
                   if pr.items.index(i) == len(pr.items)-1:
                       print("I can't find this item.")
        elif len(pr.items) < 1:
            print("I can't find this item.")
        turnUpdate()

    def do_drop(self, arg):
        """This drops an item from your inventory to the room you are. Use 'drop <obj>'."""
        pr = player.position
        #print(len(pr.items))
        if len(player.inventory) > 0:
           for i in player.inventory:
               #print(i.words)
               if arg in i.words:
                   pr.items.append(i)
                   print("You dropped %s" %i.name)
                   player.inventory.pop(player.inventory.index(i))
               else:
                   if player.inventory.index(i) == len(player.inventory)-1:
                       print("I don't think I have this item.")
        elif len(player.inventory) < 1:
            print("I don't have anything to drop.")
        turnUpdate()

    def do_use(self, arg):
        """Interact with something on the room or your inventory. Use 'use <obj> (on <obj2>)'."""
        erg = arg.split()
        arg = erg
        if len(arg) == 1:
            pr = player.position
            lit = arg
            inRes = 7
            gdRes = 7
            obtu = "a"
            for i in player.inventory:
                if arg[0] in i.words:
                    if i.usable == True:
                        i.useResult()
                    else:
                        print("I can't use this.")
                    inRes = 10
            for i in pr.items:
                if arg[0] in i.words:
                    if i.usable == True:
                        i.useResult()
                    else:
                        print("I can't use this.")
                    gdRes = 20
            if inRes + gdRes == 14:
                print("I can't seem to find this.")
        elif len(arg) == 3 and arg[1] == 'on':
            tUobj1 = "a"
            tUobj2 = "b"
            for i in player.inventory:
                if arg[0].lower() in i.words:
                    tUobj1 = i
            for i in player.position.items:
                if arg[0].lower() in i.words:
                    tUobj1 = i
            for i in player.inventory:
                if arg[2].lower() in i.words:
                    tUobj2 = i
            for i in player.position.items:
                if arg[2].lower() in i.words:
                    tUobj2 = i
            if tUobj1 != "a" and tUobj2 != "b":
                tUobj1.useI(tUobj2)
            else:
                write("I can't find at least one of these items",fast)

        elif len(arg) != 1 and len(arg) != 3:
            print("Use what?")
        turnUpdate()

    def do_unlock(self, arg): #WIP
        """Unlock things that need to be unlocked, use 'unlock <thing> with <item>'. WIP"""
        unLocc = arg.split()
        if len(unLocc) == 3 and unLocc[1] == 'with':
            if unLocc[0] == 'door':
                for i in player.position.connections:
                    if i in lockedList:
                        enn = i
                        for k in player.inventory:
                            if unLocc[2] in k.words:
                                #print(k.name)
                                #print(i)
                                #print(enn.keyItem.name)
                                if k == enn.keyItem:
                                    repEl(player.position.connections, i, player.position.connections.index(i), i.connectedRooms[0])
                                    write('You use %s, and the door unlocks.' %k.name, normal)
                                else:
                                    print("I can't open this door with %s" %k.name)


        else:
            print("Sorry, but I can't undestand that. Type 'help unlock' to see how to use this")
        turnUpdate()

    def do_read(self, arg):
        """Read the text of something that has text. Use 'read <obj>'."""
        if len(arg) > 0:
            pr = player.position
            lit = arg
            inRes = 7
            gdRes = 7
            #print(player.inventory)
            #player.lookObIn(lit)
            #pr.lookObGd(lit)
            for i in player.inventory:
                #print(i)
                #print(i.name)
                ene = {'ld':i.readable ,'wd':i.words,'rs':i.longDesc}
                #print(ene)
                if lit in ene['wd']:
                    if ene['ld'] == True:
                        write(i.texto, normal)
                    else:
                        write("I can't read this.", normal)
                    inRes = 10
            for i in pr.items:
                #print(i)
                #print(i.name)
                ene = {'ld':i.readable ,'wd':i.words,'rs':i.longDesc}
                #print(ene)
                if lit in ene['wd']:
                    if ene['ld'] == True:
                        write(i.texto, normal)
                    else:
                        write("I can't read this.", normal)
                    gdRes = 20
            if inRes + gdRes == 14:
                write("I can't seem to find this.", normal)
        else:
            print("Use what?")
        turnUpdate()

    def do_attack(self, arg):
        """Use to attack enemies, use 'attack <enemy> (with <attack>)'"""
        r = False
        if len(arg) > 0:
            arrg  = arg.split()
            if len(arrg) == 1:
                if len(player.position.enes) > 0:
                    for ens in player.position.enes:
                        if arrg[0].lower() in ens.words:
                            dam = random.randrange(punch0.damIn, punch0.damAx)
                            ens.life -= dam
                            liner = ("You attacked ",ens.selfName, " with", punch0.name, "and gave", str(dam), "damage!")
                            linToS = " ".join(liner)
                            write(linToS ,fast)
                            #print("now with ", ens.life)
                            r = True
                            if ens.life < 1:
                                write("%s died!" %ens.selfName, fast)
                                player.position.eneKill(ens)
                if r == False:
                    write("Sorry, but I don't see this enemy.", fast)
            elif len(arrg) == 3 and arrg[1] == "with":
                at = 'a'
                for att in player.attacks:
                    if arrg[2].lower() in att.name:
                        at = att
                if len(player.position.enes) > 0:
                    for ens in player.position.enes:
                        if arrg[0].lower() in ens.words and at != 'a':
                            dam = random.randrange(at.damIn, at.damAx)
                            ens.life -= dam
                            liner = ("You attacked ",ens.selfName, " with", at.name, "and gave", str(dam), "damage!")
                            linToS = " ".join(liner)
                            write(linToS ,fast)
                            #print("now with ", ens.life)
                            r = True
                            if ens.life < 1:
                                write("%s died!" %ens.selfName, fast)
                if r == False:
                    write("I don't see how I can do this.", fast)
        else:
            write("Attack? what? Use 'help attack' to see how to use this.", fast)
        turnUpdate()

    def do_open(self, arg): #Open
        """Use this to open containers, like chests. Use 'open <obj>'"""
        if len(arg) > 0:
            pr = player.position
            lit = arg
            inRes = 7
            gdRes = 7
            #print(player.inventory)
            #player.lookObIn(lit)
            #pr.lookObGd(lit)
            for i in player.inventory:
                #print(i)
                #print(i.name)
                #print(ene)
                if lit in i.words:
                    if i.tipo == 'Container':
                        i.openIt()
                    else:
                        write("I can't open this.", fast)
                    inRes = 10
            for i in pr.items:
                #print(i)
                #print(i.name)
                #print(ene)
                if lit in i.words:
                    if i.tipo == 'Container':
                        i.openIt()
                    else:
                        write("I can't open this.", fast)
                    gdRes = 20
            if inRes + gdRes == 14:
                print("I can't seem to find this.")
        else:
            write("What should I open?", fast)
        turnUpdate()
    def do_talk(self, arg): #Talk to NPCs
        """Use 'talk to <NPC>' to talk to an NPC"""
        tha = 0
        args = arg.split()
        if len(args) == 2:
            if args[0].lower() == 'to':
                for i in player.position.npcs:
                    if args[1].lower() in i.words:
                        i.talkTo()
                        tha = 4
                if tha == 0:
                    write("I can't find them.",fast)
            elif args[0].lower() != 'to':
                write("Sorry? I think you meant 'talk to'",fast)
        elif len(args) != 2:
            write("You may be actually wanting to use 'talk to <someone>'.",fast)

    def do_ttuff(self, arg): #jftsod
        """Just for testing something"""
        print('test')
        print("")
        player.position.sayNpc()
        #drUn.dialogue.conv()
        #write("Somebody once told me the world is gonna roll me.", 0.05)
        #print(player.getCarry())
        #player.position.itemsInDesc()

    def do_clear(self, arg):
        """Use this to clear the screen."""
        os.system('clear')

    do_exit = do_quit
    do_pick = do_take
    do_get = do_take

#---Text wrap stuff---
def write(text, time=normal):
    texxt = textwrap.fill(text, screenWidth)
    for character in texxt:
        sys.stdout.write(character)
        sys.stdout.flush()
        sleep(time)
    print("")

#---Title screen---
def titleScreen():
    os.system('clear')
    print("  ╔═════════════╗")
    print("  |  adv≡nture  |")
    print("  ╚═════════════╝")
    write("   ~Bottom text~ \n", normal)
    print("")
    sleep(0.08)
    print(" [start|help|quit]")
    write(" By Waces-----2018", normal)

if __name__ == '__main__':
    sleep(1.08)
    os.system('clear')
    userInput().cmdloop()
    print('\nThanks for playing!')
    sleep(2)
    os.system('clear')
else:
    os.system('clear')
    userInput().cmdloop()
    write('Thanks for playing!')
    sleep(2)
    os.system('clear')
